import logo from "./logo.svg";
import "./App.css";
import Ex_QuanLyNguoiDung from "./Ex_QuanLyNguoiDung/Ex_QuanLyNguoiDung";

function App() {
  return (
    <div className="App">
      <Ex_QuanLyNguoiDung />
    </div>
  );
}

export default App;
